Source: esda
Section: python
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               jdupes,
               pandoc <!nodoc>,
               pybuild-plugin-pyproject,
               python-mpl-sphinx-theme-doc <!nodoc>,
               python3-all,
               python3-coverage <!nocheck>,
               python3-geopandas <!nocheck>,
               python3-libpysal,
               python3-matplotlib,
               python3-nbsphinx <!nodoc>,
               python3-numpydoc <!nodoc>,
               python3-pandas,
               python3-pytest <!nocheck>,
               python3-pytest-cov <!nocheck>,
               python3-pytest-xdist <!nocheck>,
               python3-rtree,
               python3-scipy,
               python3-setuptools,
               python3-setuptools-scm,
               python3-sklearn,
               python3-sphinx <!nodoc>,
               python3-sphinx-bootstrap-theme <!nodoc>,
               python3-sphinxcontrib.bibtex <!nodoc>,
               twine
Standards-Version: 4.7.0
Homepage: https://github.com/pysal/esda
Vcs-Browser: https://salsa.debian.org/science-team/esda
Vcs-Git: https://salsa.debian.org/science-team/esda.git

Package: python3-esda
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-esda-doc
Description: Exploratory Spatial Data Analysis
 Open-source Python library for the exploratory analysis of spatial data.
 A subpackage of PySAL (Python Spatial Analysis Library), it is under active
 development and includes methods for global and local spatial autocorrelation
 analysis.
 .
 This package installs the library for Python 3.

Package: python-esda-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: Exploratory Spatial Data Analysis (common documentation)
 Open-source Python library for the exploratory analysis of spatial data.
 A subpackage of PySAL (Python Spatial Analysis Library), it is under active
 development and includes methods for global and local spatial autocorrelation
 analysis.
 .
 This package contains HTML documentation, incorporates instructions on how
 to install and configure and use this module Python esda.
